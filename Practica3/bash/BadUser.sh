#!/bin/bash
p=0
d=0;
usage="Usage: BadUser.sh [-p | -t time]"
# detecció de opcions d'entrada: només son vàlids: sense paràmetres i -p
if [ $# -ne 0 ]; then
    if [ $# -eq 1 ]; then
        if [ $1 == "-p" ]; then 
            p=1
        else
            echo $usage; exit 1
        fi 
    elif [ $# -eq 2 && $1 == "-t" ]; then 
            p=2
else 
        echo $usage; exit 1
    fi
fi
# afegiu una comanda per llegir el fitxer de password i només agafar el camp de # nom de l'usuari
for user in `cat /etc/passwd | cut -d: -f1`; do
    home=`cat /etc/passwd | grep "^$user\>" | cut -d: -f6`
    if [ -d $home ]; then
        num_fich=`find $home -type f -user $user | wc -l`
    else
        num_fich=0
    fi
    if [ $num_fich -eq 0 ]; then
        if [ $p -eq 1 || $p -eq 2]; then
# afegiu una comanda per detectar si l'usuari te processos en execució, 
# si no te ningú la variable $user_proc ha de ser 0
            user_proc=`ps aux --no-headers | grep "^$user\>" | wc -l`
            if [ $user_proc -eq 0 && $p -eq 1 ]; then
                echo "$user"
            else if [ $user_proc -eq 0 && $p -eq 2 ] 
                #esto es la nueva opcion, con extension
                        #quiero leer el arg[2] pa saber desde cuando quiero mirar
                #una opcion puede ser usar last -s
                d = $2
                if [ ${d:1} == "d" ]; then
                    #el comando que sea con fecha restandole ${d:0:1} a los dias
                    fecha = `date +%Y%m%d%H%M%S -d "-${d:0:1} days"`
                    lastlogins = `last $user -s ${fecha}`
                elif [ ${d:1} == "m" ]; then
                    fecha = `date +%Y%m%d%H%M%S -d "-${d:0:1} months"`
                    lastlogins = `last $user -s ${fecha}`
                else
                    #supongamos que son anos el else
                    fecha = `date +%Y%m%d%H%M%S -d "-${d:0:1} years"`
                    lastlogins = `last $user -s ${fecha}`
                fi
                if[ $lastlogins -eq 0 ]; then
                    echo "$user"
                fi

            fi
        else
            echo "$user"
        fi
    fi    
done
